# LEER ANTES #

Este sistema de reservas fue desarrollado con HTML5, JQuery y Materialize CSS.

### ¿Que es materialize y para que se ocupó? ###

* Hojas de estilo y colores únicamente
* Version v0.97.8
* [Ver más](http://materializecss.com/)

### Que se realizo por nuestra parte ###

* Formularios sin validación, programables por el cliente y/o su desarrollador.
* Tabs para elegir entre 1, 2, 3 o más habitaciones, segun el número que elija el cliente.
* Selector entre costos: Tarifa Prepago Online y Pago Online

### Que no se hizo ###

* No se agregaron todas las imagenes de las recamaras, ya que estas serán consumidas desde su base de datos, solo púsimos las imagenes de ejemplo que se ven en el mismo.
* No se puso la información completa ya que esta misma puede variar según el tipo de habitación, y estas serán consumidas desde su base de datos.
* Validaciones en los formularios para que usted tenga la libertad de hacerlo según el lenguaje en el que trabaje.

### ¿Dónde puede contactarnos? ###

* Para dudas, llame al 442 2136818 con Juan Carlos Ramírez
* Regrésenos un correo electrónico a carlo5@woorx.mx